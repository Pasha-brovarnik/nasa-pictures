const resultsNav = getElement('#resultsNav');
const favoritesNav = getElement('#favoritesNav');
const imagesContainer = getElement('.images-container');
const saveConfirmed = getElement('.save-confirmed');
const loader = getElement('.loader');

function getElement(selector) {
	return document.querySelector(selector);
}

// NASA API
const count = 5;
const apiKey = 'DEMO_KEY';
const apiUrl = `https://api.nasa.gov/planetary/apod?api_key=${apiKey}&count=${count}`;

let resultsArray = [];
let favorites = {};

function showContent(page) {
	window.scrollTo({ top: 0, behavior: 'instant' });
	if (page === 'results') {
		resultsNav.classList.remove('hidden');
		favoritesNav.classList.add('hidden');
	} else {
		resultsNav.classList.add('hidden');
		favoritesNav.classList.remove('hidden');
	}
	loader.classList.add('hidden');
}

function createDOMNodes(page) {
	const currentArr =
		page === 'results' ? resultsArray : Object.values(favorites);

	currentArr.forEach((result) => {
		if (!result.hdurl) {
			return;
		}
		// Card container
		const card = document.createElement('div');
		card.classList.add('card');
		// Link
		const link = document.createElement('a');
		link.href = result.hdurl;
		link.title = 'View full image';
		link.target = '_blank';
		// Image
		const image = document.createElement('img');
		image.src = result.url;
		image.alt = 'NASA picture of the day';
		image.loading = 'lazy';
		image.classList.add('card-img-top');
		// Card body
		const cardBody = document.createElement('div');
		cardBody.classList.add('card-body');
		// Card title
		const cardTitle = document.createElement('h5');
		cardTitle.classList.add('card-titile');
		cardTitle.textContent = result.title;
		// Save text
		const saveText = document.createElement('p');
		saveText.classList.add('clickable');

		if (page === 'results') {
			saveText.textContent = 'Add to favorites';
			saveText.setAttribute('onclick', `saveFavorite(${JSON.stringify(result)})`);
		} else {
			saveText.textContent = 'Remove favorite';
			saveText.setAttribute('onclick', `removeFavorite('${result.url}')`);
		}

		// Card text
		const cardText = document.createElement('p');
		cardText.textContent = result.explanation;
		// Footer container
		const footer = document.createElement('small');
		footer.classList.add('text-muted');
		// Date
		const date = document.createElement('strong');
		date.textContent = result.date;
		// Copyright
		const copyrightResult = !result.copyright ? '' : result.copyright;
		const copyright = document.createElement('span');
		copyright.textContent = ` ${copyrightResult}`;
		// Append
		footer.append(date, copyright);
		cardBody.append(cardTitle, saveText, cardText, footer);
		link.appendChild(image);
		card.append(link, cardBody);
		imagesContainer.appendChild(card);
	});
}

function updateDOM(page) {
	// Get favorites from localStorage
	if (localStorage.getItem('nasaFavorites')) {
		favorites = JSON.parse(localStorage.getItem('nasaFavorites'));
	}
	imagesContainer.textContent = '';
	createDOMNodes(page);
	const img = getElement('.card img');
	img.addEventListener('load', () => {
		showContent(page);
	});
}

// Get 10 images from NASA API
async function getNasaImages() {
	// Show loader
	loader.classList.remove('hidden');
	try {
		const response = await fetch(apiUrl);
		resultsArray = await response.json();
		updateDOM('results');
	} catch (error) {}
}

// Add result to favorite
function saveFavorite(item) {
	if (favorites[item.url]) {
		return;
	}
	// Add item to favorites
	favorites[item.url] = item;
	// Show Save confirmation for 2 seconds
	saveConfirmed.hidden = false;
	setTimeout(() => {
		saveConfirmed.hidden = true;
	}, 2000);
	// Set favorite in localStorage
	localStorage.setItem('nasaFavorites', JSON.stringify(favorites));
}

// Remove item from favorites
function removeFavorite(itemUrl) {
	if (favorites[itemUrl]) {
		delete favorites[itemUrl];
		// Set favorite in localStorage
		localStorage.setItem('nasaFavorites', JSON.stringify(favorites));
		updateDOM('favorites');
	}
}

// On page load
getNasaImages();
